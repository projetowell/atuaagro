<p align="center"><a href="https://atuaagro.com.br/" target="_blank"><img src="https://atuaagro.com.br/wp-content/uploads/2019/09/Group-164@2x.png" width="200"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## Sobre o Projeto
Projeto desenvolvido pelo cientista da computação Wellington Luiz e pelo empresário e designer Odair Oliveira

## Para rodar o projeto foi usado
#### Plugins
- Composer 2.2.5
- NPM 6.14.15
- PHP 7.3
- Laravel/framework 8.75
- Laravel/ui 3.4
- MySQL Server

#### Rodar na raiz
- composer install
- npm install
- create schema laravel (no MySQL)
- php artisan migrate
- php artisan serve

## Vulnerabilidades de segurança

Se você descobrir uma vulnerabilidade de segurança no Laravel, envie um e-mail para Well Luiz via [wellingtonluiiiz@gmail.com](mailto:wellingtonluiiiz@gmail.com). Todas as vulnerabilidades de segurança serão prontamente solucionadas.

## Licença

O framework Laravel e este código são softwares de código aberto licenciados sob a [MIT license](https://opensource.org/licenses/MIT).
